//to create a package that can used throughout the booking-app folder
package greetings

import (
	"fmt"
)

func GreetUsers(showName string, showTickets int, remainingTickets uint) {

	fmt.Println("Welcome to ShowHub Ticket App\nGet your TKTS for:", showName)
	fmt.Println("We have seating for:", showTickets, "participants and a total of:", remainingTickets, "available tickets.")

}
