//include: the main package; import fmt
//go mod init <module> to create the module/project file
//go run main.go -> to test as you add more code
//following: https://www.youtube.com/watch?v=yyUHQIec83I&t=1279s&ab_channel=TechWorldwithNana

/*	KEYWORDS
break
case	chan	const	continue
default		defer
else
fallthrough		for		func
go	goto
if	import	interface
map
package
range	return
select	struct	switch
type
var
*/

/* Numerical datatypes bitsize
	Unsigned
uint8 -> 0-255
uint16 -> 0-65535
uint32 -> 0-4294967295
uint64 -> 0-18446744073709551615
	Signed
int8 : -128 to 127
int16 : -32768 to 32767
int32 : -2147483648 to 2147483647
int64 : -9223372036854775808 to 9223372036854775807
    Decimals
float32
float64
complex64
complex128
*/

package main

import (
	"booking-app/greetings" //To use the GreetUsers func. Capitalize the first char of the func name.
	"fmt"                   //Print functionality; capabilities
	"strconv"               //To convert int <> string.
	"strings"               //For string manipulating
)

func main() {

	//Variable for static/unchangable total tickets. Use keyword: const
	const showTickets = 50

	//Variable for maintaining tickets sales
	var remainingTickets uint = 50

	//Variable for the holding a string
	var showName = "Go Booking Conference"
	incr := 3

	//Golang only uses FOR loops for all looping instances
	for {
		//Retrieve the greeting
		greetings.GreetUsers(showName, showTickets, remainingTickets)

		//Big FOR loop. The meat of the Booking App.
		//fmt.Println("Welcome to ShowHub Ticket App\nGet your TKTS for:", showName)
		//fmt.Println("We have seating for:", showTickets, "participants and a total of:", remainingTickets, "available tickets.")

		//Use (fmt)keyword: printf to display formatted data. %v to substitute the variable
		fmt.Printf("The %v is the greatest show in Earth!!!\n\n", showName)

		// More Variables & Data Types
		var fName string //You can also type: var userName = "" OR userName := ""
		var lName string
		var email string
		var ticketReq uint //You can also type: var ticketCount = 0 OR ticketReq := 0

		//Request user input: (fmt)keyword: Scanln()
		fmt.Print("Your First Name: ")
		fmt.Scanln(&fName)

		fmt.Print("Your Last Name: ")
		fmt.Scanln(&lName)

		fmt.Print("Your email address: ")
		fmt.Scanln(&email)

		//Applying a little Security check
		if len(fName) < 2 || len(lName) < 2 {
			fmt.Println("Firstname or Lastname is not valid!\nTry again ")
			break
		}

		if !strings.Contains(email, "@") {
			fmt.Println("Email is invalid!\nTry again!")
			break
		}

		fmt.Printf("Welcome %v!\n", fName)

		fmt.Print("How many tickets would you like: ")
		fmt.Scanln(&ticketReq)

		//Checking the ticketing system
		fmt.Printf("\nPlease standby %v. Checking availablity for %v ticket(s).\n", fName, ticketReq)

		//Array: Variable for tracking the ticket sales and seats available
		Seating := []string{"Jemz", "Lala"}

		//IF statements to maintain ticket requests and remaining available seats
		if remainingTickets == 0 {
			fmt.Printf("Sorry %v, We are completely sold out.\n", fName)
			break
		}
		if remainingTickets < ticketReq {

			fmt.Printf("Sorry %v, we only have %v seats available.__________~\\_(\"/)_/~_____\n\n", fName, remainingTickets)

		}
		if remainingTickets >= ticketReq {

			//A fix for the Uint to string convert. ex: strconv.Inoa(int(uint))
			Seating = append(Seating, fName+" "+lName+" | "+" seatQty: "+strconv.Itoa(int(ticketReq)))

			/* Example of a FOR loop with an applied RANGE
			for _, Seating := range Seating {

				var fanSeat = string.Fields(Seating)
				Seating = append(Seating, fanSeat[incr])
			} */

			remainingTickets -= ticketReq
			fmt.Printf("Thank you %v! \nA comfirmation email will be sent to %v\n", fName, email)
			fmt.Printf("\n\nThank you %v for you request of %v seats.\n\n", fName, ticketReq)
			fmt.Printf("You will sit next to: %v\n--------------------------------\n", Seating)
			incr += 1
		}

	} //Big FOR loop close

} //main close
