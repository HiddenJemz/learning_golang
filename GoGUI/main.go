/*  Site: Ended up being a dud on my rig.


    Will attemp to create a GUI app using GO and the previously created Booking-App

    Site: Use https://medium.com/benchkram/tutorial-adding-a-gui-golang
    RUN: go get https://github.com/Equanox/gotron

    refer to comment below
*/

package main

import (
    "github.com/Equanox/gotron"
)

func main() {
    //Create the browser window and error handling
    window, err := gotron.New()
    if err != nil{
        panic(err)
    }
    //Alter windowframe size & title
    window.WindowOptions.Width = 1200
    window.WindowOptions.Height = 980
    window.WindowOptions.Title = "ShowHub Ticket"

    /*Start the window with error handling.
        Will establish a golang <to> nodejs bridge using websockets to control
        ElectronBrowserWindow with the window object.
    */
    done, err := window.Start()
    if err != nil{
        panic(err)
    }
    
    //Wait fo the app to close
    <-done 
    
    /*  Build the gui using the terminal
        1) go build -o ./someName
        2) ./someName
    */

}//main close
