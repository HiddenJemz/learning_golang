//main.go is the common filename for all GO main functions
//'go mod init <module>'  will initiate this module/project with a go.mod file

//include the main package
package main

//import print functionality. Click the link for more info and other funcionalities.
import "fmt"

//The main function
func main() {

	fmt.Println("Hello World!!!!")

}
//Terminal: go run .
//Or click the PLAY button in the left pane.

//Date: Early 2022
